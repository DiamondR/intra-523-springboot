package ca.claurendeau.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Integer> {

	Personne findByNom(String nom);
}
