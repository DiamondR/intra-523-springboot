package ca.claurendeau.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Personne;
import ca.claurendeau.repository.PersonneRepository;

@Service
public class PersonneService {

	@Autowired
    private PersonneRepository MRepo;

	public Personne savePersonne(Personne personne) {
        return MRepo.save(personne);
    }
	public Personne createInitialPersonnes() {
		return savePersonne(new Personne("Richard", "Something"));
		
	}
	public Personne getPersonne(String nom) {
		return MRepo.findByNom(nom);
	}
	
	
}
