package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ca.claurendeau.domaine.Personne;
import ca.claurendeau.service.PersonneService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ReactControlleur {
    @Autowired
    PersonneService SPersonne;
        
    @GetMapping("/nom/{nom}")
    public Personne home(@PathVariable String nom) {
        return SPersonne.getPersonne(nom);
    }
}
